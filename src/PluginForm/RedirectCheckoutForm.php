<?php

namespace Drupal\commerce_jcc\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the class for payment off-site form.
 *
 * Provide a buildConfigurationForm() method which calls buildRedirectForm()
 * with the right parameters.
 */
class RedirectCheckoutForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * Currency map.
   *
   * @var array
   */
  private $currencyMap = [
    'EUR' => '978',
    'GBP' => '826',
    'USD' => '840',
    'CHF' => '756',
  ];

  /**
   * Http Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Logger.
   */
  private $logger;

  /**
   * RedirectCheckoutForm constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   HTTP client.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request state.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Current logger chanel.
   */
  public function __construct(Client $http_client, PrivateTempStoreFactory $temp_store_factory, RequestStack $request_stack, LoggerChannelFactoryInterface $logger) {
    $this->httpClient = $http_client;
    $this->tempStore = $temp_store_factory;
    $this->requestStack = $request_stack->getCurrentRequest()
      ->getSchemeAndHttpHost();
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('tempstore.private'),
      $container->get('request_stack'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Gateway plugin.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
   */
  private $paymentGatewayPlugin;

  /**
   * Getting plugin's configuration.
   *
   * @param string $configuration
   *   Configuration name.
   *
   * @return mixed
   *   Configuration value.
   */
  private function getConfiguration($configuration) {
    return $this->paymentGatewayPlugin->getConfiguration()[$configuration] ?? NULL;
  }

  /**
   * Get API payment URI.
   *
   * @param string $payment_url
   *   API link.
   * @param array $data
   *   Array order data.
   *
   * @return mixed
   *   API response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getPaymentLink(string $payment_url, array $data) {
    $response = $this->httpClient->post(
      $payment_url,
      [
        "form_params" => $data,
        'headers' => ['Content-type' => 'application/x-www-form-urlencoded'],
      ]
    );

    $response_data = json_decode($response->getBody(), TRUE);
    return $response_data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Payment server data.
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $data = [];
    $payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $order = $payment->getOrder();
    $payment_url = $this->getConfiguration('endpoint');
    $payment_currency = $payment->getAmount()->getCurrencyCode();
    $data['userName'] = $this->getConfiguration('user_name');
    $data['password'] = $this->getConfiguration('password');
    $unit_price = (int) $order->getTotalPrice()->getNumber() * 100;
    $data['amount'] = $unit_price;
    $data['currency'] = $this->currencyMap[$payment_currency];
    $data['orderNumber'] = $payment->getOrderId();
    // Make the relative path absolute and add the order ID.
    $data['returnUrl'] = $this->requestStack . $this->getConfiguration('return_url') . '/' . $payment->getOrderId();

    if ($fail_url = $this->getConfiguration('fail_url')) {
      // Make the relative path absolute and add the order ID.
      $data['failUrl'] = $this->requestStack . $fail_url . '/' . $payment->getOrderId();
    }
    // Get payment API link.
    $response_data = $this->getPaymentLink($payment_url, $data);

    if (isset($response_data['errorCode'])) {
      $this->logger->get('commerce_jcc')
        ->error($this->t('Payment error for @order: @message'), [
          '@order' => $data['orderNumber'],
          '@message' => $response_data['errorMessage'],
        ]);
    }

    // Store formUrl into temp store.
    $tmp_store = $this->tempStore->get("commerce_order_{$order->id()}");
    $form_url = $tmp_store->get('jcc_order_id');
    if (empty($form_url)) {
      // Set url in case if payment process is interrupted.
      if (isset($response_data['formUrl'])) {
        $tmp_store->set('jcc_order_id', $response_data['formUrl']);
        $form_url = $response_data['formUrl'];
      }
      else {
        $form_url = $data['failUrl'];
        $this->logger->get('jcc_payment')
          ->error($this->t('Payment error for @order: @message'), [
            '@order' => $data['orderNumber'],
            '@message' => $response_data['errorMessage'],
          ]);
      }
    }

    return $this->buildRedirectForm($form, $form_state, $form_url, [], self::REDIRECT_GET);
  }

}
