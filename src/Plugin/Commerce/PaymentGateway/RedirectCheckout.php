<?php

namespace Drupal\commerce_jcc\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the JccPay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "jccpay_redirect_checkout",
 *   label = @Translation("JccPay (Redirect to jcc pay)"),
 *   display_label = @Translation("JccPay"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_jcc\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = { "mastercard", "visa" },
 *   modes = {
 *     "sandbox" = @Translation("Sandbox"),
 *     "live" = @Translation("Live")
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class RedirectCheckout extends OffsitePaymentGatewayBase {

  /**
   * Payment server endpoints.
   *
   * @var string[]
   *  Server endpoints.
   */
  private $endpoints = [
    'sandbox' => 'https://gateway-test.jcc.com.cy/payment/rest/register.do',
    'live' => 'https://gateway.jcc.com.cy/payment/rest/register.do',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'user_name' => '',
      'password' => '',
      'return_url' => '',
      'fail_url' => '',
      'dynamic_callback_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['user_name'] = [
      '#title' => $this->t('User name'),
      '#description' => $this->t('Payment server login'),
      '#default_value' => $this->configuration['user_name'],
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#title' => $this->t('Password'),
      '#description' => $this->t('Payment server password'),
      '#default_value' => $this->configuration['password'],
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    $form['return_url'] = [
      '#title' => $this->t('Return URL'),
      '#description' => $this->t('The address to which the user will be
      redirected if the payment is successful. The address must be specified in
      full including the protocol used (for example, https://example.com'),
      '#default_value' => $this->configuration['return_url'],
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    $form['fail_url'] = [
      '#title' => $this->t('Fail URL'),
      '#description' => $this->t('The address to which the user is to be
      redirected in case of a failed payment.
      The address must be specified in full including the protocol used
      (for example, https://example.com'),
      '#default_value' => $this->configuration['fail_url'],
      '#type' => 'textfield',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $required_fields = [
      'user_name',
      'password',
    ];

    foreach ($required_fields as $key) {
      if (empty($values[$key])) {
        $message = t('Service is not configured for use. Please contact
        an administrator to resolve this issue.');
        $this->messenger()->addError($message);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Сhecks the mode of operation of the moduleю.
      $mode = $form_state->getValue('configuration')['jccpay_redirect_checkout']['mode'];
      $endpoint = $mode == 'live' ? $this->endpoints['live'] : $this->endpoints['sandbox'];
      $this->configuration['endpoint'] = $endpoint;
      $this->configuration['user_name'] = $values['user_name'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['return_url'] = $values['return_url'];
      $this->configuration['fail_url'] = $values['fail_url'];
    }
  }

}
