<?php

namespace Drupal\commerce_jcc\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RedirectController Check order payment.
 */
class RedirectController extends ControllerBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Logger.
   */
  private $logger;

  /**
   * The entity storage with a commerce payment.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   Entity Storage.
   */
  private $entityStorage;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   *   Request stack.
   */
  private $requestStack;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * WebpayByRedirectController constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Current logger chanel.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entityStorage
   *   Entity storage.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, EntityStorageInterface $entityStorage, RequestStack $request_stack, PrivateTempStoreFactory $temp_store_factory, ConfigFactoryInterface $config_factory) {
    $this->logger = $logger;
    $this->entityStorage = $entityStorage;
    $this->requestStack = $request_stack;
    $this->tempStore = $temp_store_factory;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $logger = $container->get('logger.factory');
    $entityStorage = $container->get('entity_type.manager')
      ->getStorage('commerce_payment');
    $request_stack = $container->get('request_stack');
    $temp_store_factory = $container->get('tempstore.private');
    $config_factory = $container->get('config.factory');


    return new static($logger, $entityStorage, $request_stack, $temp_store_factory, $config_factory);
  }

  /**
   * Payment success route function.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   Order entity.
   *
   * @return array
   *   Render Array.
   */
  public function check(OrderInterface $commerce_order) {

    $response = $this->requestStack->getCurrentRequest()->query->all();;
    $order_ids = $commerce_order->get('order_items')->getValue();
    $order_items_ids = [];
    // Get order item.
    foreach ($order_ids as $order_id) {
      $order_items_ids[] = $order_id['target_id'];
    }
    // Response data.
    $error_code = $response['errorCode'] ?? NULL;
    $error_message = $response['errorMessage'] ?? NULL;
    $jcc_order_id = $response['orderId'];

    $data = [
      'error_code' => $error_code,
      'error_message' => $error_message,
      'jcc_order_id' => $jcc_order_id,
      'order_items' => [
        '#type' => 'view',
        '#name' => 'commerce_order_item_table',
        '#display_id' => 'default',
        '#arguments' => [implode('+', $order_items_ids)],
        '#embed' => TRUE,
      ],
    ];

    // Check error.
    if (!$error_code) {
      $this->completePayment($commerce_order, $jcc_order_id);
      $data['message'] = $this->t('The payment was successful');
    }
    else {
      $data['message'] = $this->t('Payment error');
      if ($error_message) {
        $this->logger->get('commerce_jcc')
          ->error($error_message, [
            'order_id' => $commerce_order->id(),
            'jcc_order_id' => $jcc_order_id,
          ]);
      }

    }

    $render_arr = [
      '#theme' => 'commerce_jcc_pay_template',
      '#pay_response' => $data,
    ];
    return $render_arr;
  }

  /**
   * Complete new payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   An order.
   * @param string $remote_id
   *   Remote id.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function completePayment(OrderInterface $order, string $remote_id) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGateway $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->first()->entity;

    // Сheck whether the payment was made.
    $query = $this->entityStorage->getQuery();
    $query->condition('order_id', $order->id());
    $query->accessCheck(FALSE);
    $payment_data = $query->execute();
    $is_paid = $this->entityStorage->loadMultiple($payment_data);

    // Do not create a new payment if there was a payment.
    if (!count($is_paid) > 0) {
      $payment = $this->entityStorage->create([
        'state' => 'completed',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $payment_gateway->id(),
        'order_id' => $order->id(),
        'remote_id' => $remote_id,
      ]);
      $payment->save();

      $message = $this->t('New payment for order #@order', ['@order' => $order->id()]);
      $this->logger->get('commerce_jcc')->notice($message, [
        'link' => $order->toLink('Order')->toString(),
      ]);

      // Clean up tmp data.
      $tmp_store = $this->tempStore->get("commerce_order_{$order->id()}");
      $tmp_store->delete('jcc_order_id');
    }
  }

  /**
   * Payment error output.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *
   * @return array
   */
  public function payError(OrderInterface $commerce_order) {

    $site_mail = $this->configFactory->get('system.site')->get('mail');
    $error_message = $this->t('Payment error for the order #@order. To resolve a payment issue, please email us.', [
      '@order' => $commerce_order->id(),
    ]);

    $this->logger->get('commerce_jcc')
      ->error($error_message, [
        'order_id' => $commerce_order->id(),
      ]);

    $data = [
      'error_message' => $error_message,
      'error_mail' => $site_mail,
    ];

    $render_arr = [
      '#theme' => 'commerce_jcc_pay_template',
      '#pay_response' => $data,
    ];
    return $render_arr;
  }

  /**
   * Changing the title on the payment page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   Order Entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Page title.
   */
  public function getTitle(OrderInterface $commerce_order) {
    $ref_id = $commerce_order->get('field_reference_id')->value;
    $title = $this->t('Order #@order', ['@order' => $ref_id]);
    return $title;
  }
}
