# Commerce JCC Payments

Commerce JCC Payments module is an integration module with the [JCC payment API](https://gateway.jcc.com.cy/developer/).

### Module settings

- After activating the module, add new Payment gateways in the settings Drupal commerce
- In the plugin block, select JccPay
- The mode block allows you to choose a test or live mode
- Enter your login and password
- **Return URL** - The relative reference address to which the user will be redirected if the payment is successful. For example an example (/buyer/payment_redirect)
- **Fail URL** - The relative to which the user is to be redirected in case of a failed payment. For example an example (/buyer/payment_redirect_fail)
